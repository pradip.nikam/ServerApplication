/**
 * 
 */
package com.study.demo.entity;

/**
 * @author Pradip
 *
 */
public class GenericResponseModel {
	
	private String responseMessage;
	private Object resultObject;
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public Object getResultObject() {
		return resultObject;
	}
	public void setResultObject(Object resultObject) {
		this.resultObject = resultObject;
	}
	@Override
	public String toString() {
		return "GenericResponseModel [responseMessage=" + responseMessage + ", resultObject=" + resultObject + "]";
	}
	
	

}
