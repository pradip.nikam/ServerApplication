/**
 * 
 */
package com.study.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.study.demo.dao.EmployeeDAO;
import com.study.demo.model.Employee;

/**
 * @author Pradip
 *
 */
@Service
public class EmployeeServiceImpl implements IEmployeeService {

	@Autowired
	EmployeeDAO dao;

	@Override
	public Employee getEmpDetails(String empName) {

		return dao.findByempName(empName);
	}

	@Override
	public void insertEmpRecord(Employee e) {
		dao.insert(e);

	}

}
