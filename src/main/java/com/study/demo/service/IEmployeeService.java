/**
 * 
 */
package com.study.demo.service;

import com.study.demo.model.Employee;

/**
 * @author Pradip
 *
 */
public interface IEmployeeService {
	
	public Employee getEmpDetails(String empId);
	
	public void insertEmpRecord(Employee e);

}
