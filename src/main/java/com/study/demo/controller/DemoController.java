/**
 * 
 */
package com.study.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.study.demo.entity.GenericResponseModel;
import com.study.demo.model.Employee;
import com.study.demo.service.IEmployeeService;

/**
 * @author Pradip
 *
 */
@RestController
@RequestMapping("/api")
public class DemoController {
	@Autowired
	IEmployeeService empService;
	
	@PostMapping(value="/saveEmployee",produces = "application/json")
	public void insertEmployeRecord(@RequestBody Employee emp) {	
		System.out.println("inside insertEmployeRecord..."+emp);
		if(null!=emp) {
			empService.insertEmpRecord(emp);
			System.out.println("Record has been successfully inserted");
		}
		
	}
	@GetMapping(value="/getEmployeeDetails",produces = "application/json")
	public ResponseEntity<GenericResponseModel> getEmployeeDetails(@RequestParam(value="empName") String empName) {	
		System.out.println("inside getEmployeeDetails empName: ..."+empName);
		GenericResponseModel response=new GenericResponseModel();
			Employee result=empService.getEmpDetails(empName);
			System.out.println("Record has been successfully fetched.."+result);
			if(null!=result) {
				response.setResponseMessage("Success");
				response.setResultObject(result);
				return new ResponseEntity<>(response,HttpStatus.OK);
			}
			return new ResponseEntity<>(response,HttpStatus.OK);
		
	}
	

}
