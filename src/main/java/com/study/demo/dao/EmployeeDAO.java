/**
 * 
 */
package com.study.demo.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.study.demo.model.Employee;

/**
 * @author Pradip
 *
 */

public interface EmployeeDAO extends MongoRepository<Employee, String> {
	
	@Query("{ empName: ?0}")
	public Employee findByempName(String empName);
	
	
}
