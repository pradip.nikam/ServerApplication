package com.study.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.study.demo.dao.EmployeeDAO;

@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, 
	    DataSourceTransactionManagerAutoConfiguration.class, 
	    HibernateJpaAutoConfiguration.class},scanBasePackageClasses = EmployeeDAO.class)
@EnableMongoRepositories
@ComponentScan(basePackages = {"com.study.demo"})
public class SpringAngularApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SpringAngularApplication.class, args);
		System.out.println("VVVVVVVVVVVVVVV.........SpringAngularApplication Started Succesfully......VVVVVVVVVVVVV");
		
	
	}

}
